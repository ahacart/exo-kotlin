package compterMots

fun compterMots(phrase: String): Int {
    val asciiSpace: Array<Char> =  arrayOf('\t', '\n', '\b', '\r', ' ')
    var nb = 0
    var i = 0
    var motStart = 0
    // Suppression des espaces du début
    while ((i < phrase.length) && (asciiSpace.contains(phrase[i]))) {
        i++
    }
    while (i < phrase.length)  {
        if (asciiSpace.contains(phrase[i]).not()) {
            i++
            continue
        }
        nb++
        i++
        while ((i < phrase.length) && (asciiSpace.contains(phrase[i]))) {
            i++
        }
        motStart = i
    }
    if (motStart < phrase.length) {
        nb++
    }
    return nb
}